import { Series } from "./series";

export class Item {
    constructor(
        public author_id: string,
        public series: Series[]
    ) {}
}
