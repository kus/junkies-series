export class Series {
    constructor(
        public episode: number,
        public season: number,
        public series_id: string
    ) {

    }
}
