import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing/app-routing.module';
import { AppComponent } from './app.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule, AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';
import { HomeComponent } from './app-routing/home/home.component';
import { AboutComponent } from './app-routing/about/about.component';
import { MovieDbService } from './services/movie-db.service';
import { AuthService } from './services/auth.service';
import { SeoService } from './services/seo.service';
import { MatCardModule, MatSnackBarModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {MatMenuModule} from '@angular/material/menu'; 
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent
  ],
  imports: [
    MatMenuModule,
    MatCardModule,
    BrowserAnimationsModule,
    MatSnackBarModule,
    BrowserModule,
    FormsModule,
    // HttpModule,
    AppRoutingModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features
  ],
  providers: [MovieDbService, AuthService, AngularFirestore, SeoService],
  bootstrap: [AppComponent]
})
export class AppModule { }